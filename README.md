# [Simple Personal Website Template](https://east.codeberg.page/SPWT/) </br>

## Features </br>
* Easy page navigation 
* Page section links
* Image gallery
* Blog

## Ways to host</br>
1) [Codeberg or Github pages](https://docs.codeberg.org/codeberg-pages/) </br>
<em>No domain required & custom domains supported all for free.</em>
2) Self hosting using [NGINX](https://nginx.org)

## Q&A
>"What is SPWT?"

A <b>S</b>imple, <b>P</b>ersonal, <b>W</b>ebsite, <b>T</b>emplate for you to do what you wish with it.

>"The HTML is messy / You did something wrong."

The purpose while creating was to be legible enough for me.</br>
I'm not following any orginizational standards, this is just mostly meant to be a fuctional site.</br>
If a better or more organized solution exists [email me](mailto:east@eastt.org)! Alternatively create a [pull](https://codeberg.org/east/SPWT/pulls) or [issue](https://codeberg.org/east/SPWT/issues/new) request.</br>

### Credits, inspiration, and additional resources. </br><em>
[Openmoji](https://openmoji.org/) </br>
[Lightgallery](https://www.lightgalleryjs.com/) </br>
[Brutalist-web.design](https://brutalist-web.design/) </br>
[Bestmotherfucking.website](https://bestmotherfucking.website/) </br></em>